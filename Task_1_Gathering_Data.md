<img src="images/IDSNlogo.png" width="200" height="200"/>

# Task 1: Gathering Data

## Task 1: Gathering data

In order to train your Visual Recognition Classifier, you need create and train an image classifier using the notebook included in this exercise. Collecting the right and diversified data is a very important step for building a good vision recognition model. You can use the images at the source below or you can source your own.  


[https://www.kaggle.com/kmader/satellite-images-of-hurricane-damage?](https://www.kaggle.com/kmader/satellite-images-of-hurricane-damage?)

During your images collection process, we recommend you to:

1.  Collect at least 160 images for each set of labels (that is 160 images for damage, 160 for no damage)

2.  Keep around 5% (approx. 10 per label) of the collected images for testing the model

3.  For the remainder 95% of the images (that is, about 150 per label) that will be used for training the model, create two different ".zip" files. Call them hurricane-damage.zip and hurricante-no-damage.zip. This will make it easy for drag-and-drop into Watson Visual Recognition. Note that if you use the Kaggle source above, the data set already is divided for you though you will need to remove images from each zip file so they’re not too large. 






## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-10-09 | 2.0 | Arpita | Migrated Lab to Markdown and added to course repo in GitLab |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
